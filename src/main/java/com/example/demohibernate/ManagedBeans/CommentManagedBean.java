package com.example.demohibernate.ManagedBeans;


import com.example.demohibernate.Entities.Comment;
import com.example.demohibernate.Entities.Post;
import com.example.demohibernate.Entities.User;
import com.example.demohibernate.Repositories.PostRepository;
import com.example.demohibernate.Repositories.UsersRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named
@ViewScoped
@Transactional(rollbackFor = RuntimeException.class)
public class CommentManagedBean {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Getter
    @Setter
    private Comment comment;

    @Getter
    private User user;

    @PostConstruct
    public void init() {
        comment = new Comment();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        user = usersRepository.findByUserName(auth.getName());
    }

    public void saveComment(Comment comment, Long id) {
        try {
            Post post = postRepository.findOne(id);
            comment.setCommenter(user);
            comment.setCreatedDate(new Date());
            comment.setLastModifiedDate(new Date());

            post.getComments().add(comment);
            postRepository.save(post);
            this.comment = new Comment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteComment(Comment comment, Long id) {
        try {
            Post post = postRepository.findOne(id);

            List<Comment> commentList = post.getComments();
            for (Comment comment1 : commentList) {
                if (comment1.getId().equals(comment.getId())) {
                    commentList.remove(comment1);
                    break;
                }
            }
            post.setComments(commentList);
            postRepository.save(post);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
