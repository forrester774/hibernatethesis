package com.example.demohibernate.ManagedBeans;


import com.example.demohibernate.Entities.Post;
import com.example.demohibernate.Entities.Search;
import com.example.demohibernate.Repositories.CustomPostRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class SearchManagedBean {

    @Autowired
    private CustomPostRepository customPostRepository;

    @Getter
    private List<Post> foundByKeyword;

    @Getter
    private double searchByKeywordTime;

    @Getter
    @Setter
    private Search search;

    @PostConstruct
    public void init() {
        foundByKeyword = new ArrayList<>();
        search = new Search();
    }
    @Transactional(rollbackFor = RuntimeException.class, readOnly = true)
    public void fuzzySearchByKeyword() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            foundByKeyword = customPostRepository.fuzzySearchByKeyword(search.getKeyword());
            stopWatch.stop();
            searchByKeywordTime = stopWatch.getTotalTimeSeconds();
            search = new Search();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional(rollbackFor = RuntimeException.class, readOnly = true)
    public void searchByKeyword() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            foundByKeyword = customPostRepository.searchByKeyword(search.getKeyword());
            stopWatch.stop();
            searchByKeywordTime = stopWatch.getTotalTimeSeconds();
            search = new Search();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
