package com.example.demohibernate.ManagedBeans;


import com.example.demohibernate.Entities.Post;
import com.example.demohibernate.Entities.User;
import com.example.demohibernate.Repositories.PostRepository;
import com.example.demohibernate.Repositories.UsersRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
@Transactional(rollbackFor = RuntimeException.class)
public class MyPostsManagedBean {

    @Getter
    @Autowired
    private PostRepository postRepository;

    @Getter
    @Setter
    private List<Post> postsList;

    @Getter
    private User user;

    @Autowired
    private UsersRepository usersRepository;

    @PostConstruct
    public void init() {
        postsList = new ArrayList<>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        user = usersRepository.findByUserName(auth.getName());
        loadPosts();

    }

    private void loadPosts() {
        postsList = postRepository.findAllByPosterEquals(getUser());
    }

    public void deletePost(Long id) {
        try {
            postRepository.delete(id);
            loadPosts();
            addMessage("Post Deleted!");
        } catch (Exception e) {
            addMessage("Error deleting post! Error message: " + e);
        }
    }

    private void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
