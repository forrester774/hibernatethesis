package com.example.demohibernate.ManagedBeans;


import com.example.demohibernate.Entities.*;
import com.example.demohibernate.Repositories.PostRepository;
import com.example.demohibernate.Repositories.SocialRepository;
import com.example.demohibernate.Repositories.UsersRepository;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.*;

@Named
@SessionScoped
@Transactional(rollbackFor = RuntimeException.class)
public class PostManagedBean {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private SocialRepository socialRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Getter
    private User user;

    @Getter
    @Setter
    private Post post;

    @Getter
    @Setter
    private List<String> topics;

    @PostConstruct
    public void init() {
        post = new Post();
        topics = findAllTopics();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        user = usersRepository.findByUserName(auth.getName());
    }

    public void savePost(Post post) {
        try {
            if (post.getId() == null) {
                post.setCreatedDate(new Date());
                post.setLastModifiedDate(new Date());
                post.setPoster(user);
                postRepository.save(post);
                topics = findAllTopics();
                addMessage("Post Saved!");

            } else {
                post.setLastModifiedDate(new Date());
                postRepository.update(post);
                topics = findAllTopics();
                addMessage("Post Updated!");
            }
        } catch (Exception e) {
            addMessage("Error saving post! Error message:" + e);
        }
    }

    public void updatePost(Long id) {
        try {
            post = postRepository.findOne(id);

            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

            Map<String, Object> requestMap = externalContext.getRequestMap();
            requestMap.put("post", post);
            FacesContext.getCurrentInstance().getExternalContext().redirect("https://nameless-mesa-66814.herokuapp.com/editPost.xhtml");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openPost(Long id) {
        try {
            post = postRepository.findOne(id);

            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> requestMap = externalContext.getRequestMap();
            requestMap.put("posts", post);
            FacesContext.getCurrentInstance().getExternalContext().redirect("https://nameless-mesa-66814.herokuapp.com/viewPost.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void newPost() throws Exception {
        try {
            post = new Post();
            Social social = new Social();
            post.setComments(new ArrayList<>());
            post.setSocial(social);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().clear();
            FacesContext.getCurrentInstance().getExternalContext().redirect("https://nameless-mesa-66814.herokuapp.com/editPost.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void myPosts() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().clear();
            FacesContext.getCurrentInstance().getExternalContext().redirect("https://nameless-mesa-66814.herokuapp.com/myPosts.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void imageUpload(FileUploadEvent event) {
        try {
            post.setImage(new Image(event.getFile().getContents(),event.getFile().getFileName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissImage() {
        post.setImage(null);
    }

    public String getImageContentsAsBase64(Long id) {
        post = postRepository.findOne(id);
        if (post.getImage() != null) {
            return Base64.getEncoder().encodeToString(post.getImage().getBytes());
        }
        return null;
    }

    public boolean hasImage(Long id) {
        post = postRepository.findOne(id);
        return post.getImage() != null;
    }

    private void simpleSave(Post post) {
        postRepository.save(post);
    }

    public void upvote(Long id) {
        try {
            post = postRepository.findOne(id);
            Social social = socialRepository.findOne(post.getSocial().getId());
            List<SocialUserHandler> userHandlers = social.getUserHandler();
            List<String> alreadyVoted = new ArrayList<>();
            for (SocialUserHandler userHandler : userHandlers) {
                alreadyVoted.add(userHandler.getUserName());
            }
            if(!alreadyVoted.contains(user.getUserName())) {
                SocialUserHandler newUserHandler = new SocialUserHandler();
                newUserHandler.setUserName(user.getUserName());
                newUserHandler.setDecision(true);
                social.setUpvotes(social.getUpvotes() + 1);
                social.getUserHandler().add(newUserHandler);
                post.setSocial(social);
                simpleSave(post);
            }
            if(alreadyVoted.contains(user.getUserName())){
                SocialUserHandler userHandler = usersRepository.findOne(user.getUserName());
                if(userHandler.isDecision()){
                    usersRepository.deleteUserHandler(userHandler.getUserName());
                    social.setUpvotes(social.getUpvotes() - 1);
                    social.getUserHandler().remove(userHandler);
                    simpleSave(post);
                } else {
                    userHandler.setDecision(true);
                    social.setUpvotes(social.getUpvotes() + 1);
                    social.setDownvotes(social.getDownvotes() - 1);
                    simpleSave(post);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void downvote(Long id) {
        try {
            post = postRepository.findOne(id);
            Social social = socialRepository.findOne(post.getSocial().getId());
            List<SocialUserHandler> userHandlers = social.getUserHandler();
            List<String> alreadyVoted = new ArrayList<>();
            for (SocialUserHandler userHandler : userHandlers) {
                alreadyVoted.add(userHandler.getUserName());
            }
            if(!alreadyVoted.contains(user.getUserName())) {
                SocialUserHandler newUserHandler = new SocialUserHandler();
                newUserHandler.setUserName(user.getUserName());
                newUserHandler.setDecision(false);
                social.setDownvotes(social.getDownvotes() + 1);
                social.getUserHandler().add(newUserHandler);
                post.setSocial(social);
                simpleSave(post);
            }
            if(alreadyVoted.contains(user.getUserName())){
                SocialUserHandler userHandler = usersRepository.findOne(user.getUserName());
                if(!userHandler.isDecision()){
                    usersRepository.deleteUserHandler(userHandler.getUserName());
                    social.setDownvotes(social.getDownvotes() - 1);
                    social.getUserHandler().remove(userHandler);
                    simpleSave(post);
                } else {
                    userHandler.setDecision(false);
                    social.setDownvotes(social.getDownvotes() + 1);
                    social.setUpvotes(social.getUpvotes() - 1);
                    simpleSave(post);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<String> findAllTopics() {

        List<Post> posts = postRepository.findAll();

        List<String> toReturn = new ArrayList<>();
        for (Post post : posts) {
            {
                if (!toReturn.contains(post.getTopic())) {
                    toReturn.add(post.getTopic());
                }
            }
        }
        return toReturn;

    }
}
