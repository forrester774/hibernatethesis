package com.example.demohibernate.ManagedBeans;


import com.example.demohibernate.Entities.Post;
import com.example.demohibernate.Repositories.CustomPostRepository;
import com.example.demohibernate.Repositories.PostRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class HomeManagedBean {

    @Getter
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CustomPostRepository customPostRepository;

    @Setter
    @Getter
    private List<Post> postsList;

    @Getter
    private double loadPostsTime;

    @Getter
    private double findPostsByTopicsTime;

    @Getter
    private double mostLikedTime;

    @Getter
    private List<Post> postsByTopic;

    @Getter
    private List<Post> mostLiked;

    @PostConstruct
    public void init() {
        postsList = new ArrayList<>();
        loadPosts();
        postsByTopic = new ArrayList<>();
        mostLiked = new ArrayList<>();
    }
    @Transactional(rollbackFor = RuntimeException.class, readOnly = true)
    private void loadPosts() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        postsList = postRepository.findAll();
        stopWatch.stop();
        loadPostsTime = stopWatch.getTotalTimeSeconds();
    }

    public void homeNav() throws Exception{
        FacesContext.getCurrentInstance().getExternalContext().redirect("https://nameless-mesa-66814.herokuapp.com/index.xhtml");
    }

    @Transactional(rollbackFor = RuntimeException.class, readOnly = true)
    public void findPostsByTopics(String topic) {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            postsByTopic = postRepository.findAllByTopicsContains(topic);
            stopWatch.stop();
            findPostsByTopicsTime = stopWatch.getTotalTimeSeconds();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Transactional(rollbackFor = RuntimeException.class, readOnly = true)
    public void mostLiked() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            mostLiked = customPostRepository.upvotes();
            stopWatch.stop();
            mostLikedTime = stopWatch.getTotalTimeSeconds();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
