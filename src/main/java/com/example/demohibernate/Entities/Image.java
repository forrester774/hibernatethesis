package com.example.demohibernate.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class Image extends AbstractEntity implements Serializable {

    @Column(name = "bytestream")
    private byte[] bytes;

    @Column(name = "file_name")
    private String fileName;
}
