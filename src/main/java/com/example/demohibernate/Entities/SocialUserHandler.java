package com.example.demohibernate.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "User_Handler")
@Entity
public class SocialUserHandler extends AbstractEntity {

    @Column(name = "user_name")
    private String userName;

    @Column
    private boolean decision;
}
