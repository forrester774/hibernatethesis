package com.example.demohibernate.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Indexed
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "posts", indexes = {@javax.persistence.Index(columnList = "title", name = "title"),
        @javax.persistence.Index(columnList = "description", name = "description"),
@javax.persistence.Index(columnList = "content", name = "content")})
public class Post extends AbstractEntity implements Serializable {


    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.YES )
    @NotEmpty(message = "Every post must have a title!")
    @Size(max = 20, min = 3, message = "Title must be min 3 max 20 letters long!")
    @Column(name = "title")
    private String title;

    @Field(index = Index.YES, analyze = Analyze.NO,  store = Store.YES)
    @NotEmpty(message = "Every post must have a description!")
    @Size(max = 200, min = 5, message = "Description must be  min 5, max 200 letters long!")
    @Column(name = "description")
    private String description;

    @Field(index = Index.YES, analyze = Analyze.NO,  store = Store.YES)
    @Column(name = "content")
    @Size(max = 10000, min = 5, message = "Content must be min 5 max 10000 letters long!")
    @NotEmpty(message = "Every post must have some content!")
    private String content;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "post_id")
    private List<Comment> comments;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "social_id")
    private Social social;

    @OneToOne
    @JoinColumn(name = "poster_id")
    private User poster;

    @Column(name = "topic")
    @NotEmpty(message = "Every post must have a topic!")
    private String topic;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "image_id")
    private Image image;
}
