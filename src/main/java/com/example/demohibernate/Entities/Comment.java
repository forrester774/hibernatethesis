package com.example.demohibernate.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
public class Comment extends AbstractEntity implements Serializable {


    @Size(max = 100)
    @Column(name = "comment")
    private String commentBody;

    @JoinColumn(name = "user_id")
    private User commenter;

}
