package com.example.demohibernate.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@Entity
public class User extends AbstractEntity implements Serializable {

    @Column(name = "name")
    private String name;

    @NotEmpty(message = "Please enter a username!")
    @Column(name = "username")
    private String userName;

    @Length(min = 5, message = "The password must have at least 5 characters!")
    @NotEmpty(message = "Please enter a password!")
    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;
}
