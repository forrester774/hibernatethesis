package com.example.demohibernate.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "social")
@Entity
public class Social extends AbstractEntity implements Serializable {

    @Column(name = "upvote")
    private int upvotes;

    @Column(name = "downvote")
    private int downvotes;

    @JoinColumn(name = "social_id")
    @OneToMany(cascade = CascadeType.ALL)
    private List<SocialUserHandler> userHandler;

}
