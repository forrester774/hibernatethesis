package com.example.demohibernate.Repositories.impl;


import com.example.demohibernate.Entities.Post;
import com.example.demohibernate.Repositories.CustomPostRepository;
import org.apache.lucene.search.Query;
import org.hibernate.CacheMode;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CustomPostRepositoryImpl implements CustomPostRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Post> upvotes() {
        return (List<Post>)  em.createQuery( "select p from Post as p inner join p.social as social order " +
                "by " +
                "social.upvotes desc ").setMaxResults(5).getResultList();
    }

    @Override
    public List<Post> fuzzySearchByKeyword(String keyword) throws Exception {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        fullTextEntityManager.createIndexer().batchSizeToLoadObjects(20000).cacheMode(CacheMode.IGNORE)
                .threadsToLoadObjects(10)
                .idFetchSize(500).startAndWait();

        QueryBuilder builder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Post.class).get();

        Query fullTextQuery =
                builder.keyword().fuzzy().withEditDistanceUpTo(2).withPrefixLength(0).onField("content").andField(
                "description").andField(
                "title").matching(keyword).createQuery();

        return (List<Post>) fullTextEntityManager.createFullTextQuery(fullTextQuery).getResultList();
    }

    @Override
    public List<Post> searchByKeyword(String keyword) throws Exception {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        fullTextEntityManager.createIndexer().startAndWait();

        QueryBuilder builder =
                fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Post.class).get();

        Query fullTextQuery =
                builder.keyword().onField("content").andField(
                        "description").andField(
                        "title").matching(keyword).createQuery();

        return (List<Post>) fullTextEntityManager.createFullTextQuery(fullTextQuery).getResultList();
    }
}
