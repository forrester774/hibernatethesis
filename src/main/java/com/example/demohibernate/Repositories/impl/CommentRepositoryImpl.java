package com.example.demohibernate.Repositories.impl;

import com.example.demohibernate.Entities.Comment;
import com.example.demohibernate.Repositories.CommentRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CommentRepositoryImpl implements CommentRepository {


    @PersistenceContext
    private EntityManager em;

    @Override
    public void save(Comment comment) {
        em.persist(comment);
    }
}
