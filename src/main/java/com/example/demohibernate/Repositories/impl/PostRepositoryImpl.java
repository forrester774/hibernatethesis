package com.example.demohibernate.Repositories.impl;

import com.example.demohibernate.Entities.Post;
import com.example.demohibernate.Entities.User;
import com.example.demohibernate.Repositories.PostRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PostRepositoryImpl implements PostRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Post> findAllByTopicsContains(String topic) {
        return (List<Post>) em.createQuery("select p from Post p where p.topic=:topic order by p.createdDate desc").setParameter("topic",
                topic).getResultList();
    }

    @Override
    public List<Post> findAllByPosterEquals(User user) {
        return (List<Post>) em.createQuery("select p from Post p where poster_id=:id").setParameter("id",
                user.getId()).getResultList();

    }

    @Override
    public Post findOne(Long id) {
        return (Post) em.createQuery("select p from Post p where id=:id").setParameter("id", id).getSingleResult();
    }

    @Override
    public void save(Post post) {
        em.persist(post);
    }

    @Override
    public void update(Post post) {
        em.merge(post);
    }

    @Override
    public List<Post> findAll() {
        return (List<Post>) em.createQuery("select p from Post p order by p.createdDate desc").getResultList();
    }

    @Override
    public void delete(Long id) {
        em.remove(findOne(id));
    }
}
