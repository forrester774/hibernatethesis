package com.example.demohibernate.Repositories.impl;

import com.example.demohibernate.Entities.Social;
import com.example.demohibernate.Repositories.SocialRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class SocialRepositoryImpl implements SocialRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void save(Social social) {
        em.persist(social);
    }

    @Override
    public Social findOne(Long id) {
        return (Social) em.createQuery("select s from Social s where id =:id").setParameter("id", id).getSingleResult();
    }
}
