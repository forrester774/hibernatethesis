package com.example.demohibernate.Repositories.impl;

import com.example.demohibernate.Entities.SocialUserHandler;
import com.example.demohibernate.Entities.User;
import com.example.demohibernate.Repositories.UsersRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class UsersRepositoryImpl implements UsersRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public User findByUserName(String username) {
       return (User) em.createQuery("select u from User u where username=:username").setParameter("username",
               username).getSingleResult();
    }

    @Override
    public void save(User user) {
        em.persist(user);
    }


    @Override
    public SocialUserHandler findOne(String userName) {
        return (SocialUserHandler) em.createQuery("select s from SocialUserHandler s where userName=:userName").setParameter("userName", userName).getSingleResult();
    }

    @Override
    public void deleteUserHandler(String userName) {
        em.remove(em.createQuery("select u from SocialUserHandler u where userName=:userName").setParameter("userName", userName).getSingleResult());
    }


}
