package com.example.demohibernate.Repositories;


import com.example.demohibernate.Entities.Comment;


public interface CommentRepository {

    void save(Comment comment);
}
