package com.example.demohibernate.Repositories;


import com.example.demohibernate.Entities.SocialUserHandler;
import com.example.demohibernate.Entities.User;


public interface UsersRepository {

    User findByUserName(String username);

    void save(User user);

    SocialUserHandler findOne(String name);

    void deleteUserHandler(String userName);
}
