package com.example.demohibernate.Repositories;


import com.example.demohibernate.Entities.Post;
import com.example.demohibernate.Entities.User;

import java.util.List;


public interface PostRepository {

    List<Post> findAllByTopicsContains(String topic);

    List<Post> findAllByPosterEquals(User user);

    Post findOne(Long id);

    void save(Post post);

    void update(Post post);

    List<Post> findAll();

    void delete(Long id);
}
