package com.example.demohibernate.Repositories;


import com.example.demohibernate.Entities.Post;

import java.util.List;

public interface CustomPostRepository {

    List<Post> upvotes();

    List<Post> fuzzySearchByKeyword(String keyword) throws  Exception;

    List<Post> searchByKeyword(String keyword) throws  Exception;

}
