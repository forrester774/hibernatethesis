package com.example.demohibernate.Repositories;


import com.example.demohibernate.Entities.Social;


public interface SocialRepository {

    void save(Social social);

    Social findOne(Long id);
}
